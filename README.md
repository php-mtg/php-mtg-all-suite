# php-mtg/php-mtg-all-suite

Metapackage that lists all the packages under the php-mtg namespace

![coverage](https://gitlab.com/php-mtg/php-mtg-all-suite/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-mtg/php-mtg-all-suite ^8`


## Included packages

| Package Pipeline | Package Coverage | Package Name |
|:-----------------|:-----------------|:-------------|
| ![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-interface/badges/master/pipeline.svg?style=flat-square) | | [php-mtg/mtg-api-com-mtgjson-interface](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-interface) |
| ![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-object/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-object/badges/master/coverage.svg?style=flat-square) | [php-mtg/mtg-api-com-mtgjson-object](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-object) |
| ![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgstocks-interface/badges/master/pipeline.svg?style=flat-square) | | [php-mtg/mtg-api-com-mtgstocks-interface](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-interface) |
| ![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgstocks-object/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/php-mtg/mtg-api-com-mtgstocks-object/badges/master/coverage.svg?style=flat-square) | [php-mtg/mtg-api-com-mtgstocks-object](https://gitlab.com/php-mtg/mtg-api-com-mtgstocks-object) |
| ![coverage](https://gitlab.com/php-mtg/mtg-api-com-scryfall-interface/badges/master/pipeline.svg?style=flat-square) | | [php-mtg/mtg-api-com-scryfall-interface](https://gitlab.com/php-mtg/mtg-api-com-scryfall-interface) |
| ![coverage](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object/badges/master/coverage.svg?style=flat-square) | [php-mtg/mtg-api-com-scryfall-object](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object) |
| ![coverage](https://gitlab.com/php-mtg/php-keyrune-bridge/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/php-mtg/php-keyrune-bridge/badges/master/coverage.svg?style=flat-square) | [php-mtg/php-keyrune-bridge](https://gitlab.com/php-mtg/php-keyrune-bridge) |
| ![coverage](https://gitlab.com/php-mtg/php-mana-bridge/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/php-mtg/php-mana-bridge/badges/master/coverage.svg?style=flat-square) | [php-mtg/php-mana-bridge](https://gitlab.com/php-mtg/php-mana-bridge) |


## License

MIT (See [license file](LICENSE)).
